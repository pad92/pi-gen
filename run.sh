#!/bin/sh

touch ./stage4/SKIP ./stage4/SKIP_IMAGES ./stage5/SKIP ./stage5/SKIP_IMAGES
cp ./stage4/EXPORT_IMAGE ./stage3/
echo "IMG_NAME='motioneye'" > config
echo "ENABLE_SSH=1"                     >> config
CONTINUE=1 ./build-docker.sh
(cd deploy; sha256sum *.info image*.zip > sha256sums)
